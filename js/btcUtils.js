/**
 * Created by alessandrofrancia on 16/09/16.
 */
(function () {
    window.BTCUtils = function () {
        function buildError(code, description) {
            var error = {};
            error.errorCode = code;
            error.description = description;
            return error;
        }

        function buildResponse(response) {
            if (this.error) {
                return this.error;
            } else {
                return {
                    errorCode: 0,
                    data: response
                }
            }

        }


        var mVideo;
        var mStream;
        var mVideoID;
        var mRefreshTime;
        var chartApiURL = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=";
        return {
            encode_qrcode_URL: function (bitcoinAddress, opts) {
                for (var prop in opts) {
                    if (opts.hasOwnProperty(prop)) {
                        if (opts[prop] == undefined || opts[prop] == null || opts[prop] == "") {
                            delete opts[prop];
                        }
                    }
                }
                try {
                    var response = bip21.encode(bitcoinAddress, opts);
                    return buildResponse(response);
                } catch (err) {
                    return buildError(-1, err.message);
                }
            },
            decode_qrcode_URL: function (URL) {
                try {
                    var response = bip21.decode(URL);

                    return buildResponse(response);
                } catch (err) {
                    return buildError(-1, err.message);
                }
            },
            buildQrcode: function (URI) {
                return chartApiURL + URI;
            },
            stopVideo: function () {
                mVideo.pause();
                mVideo.src = "";
                mStream.getVideoTracks()[0].stop();
            },

            startStream: function (cameraID, callback) {
                if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                    var qr = new QrCode();

                    var opt = {video: {deviceId: cameraID ? {exact: cameraID} : true}};
                    // Not adding `{ audio: true }` since we only want video now
                    navigator.mediaDevices.getUserMedia(opt).then(function (stream) {
                        mStream = stream;
                        mVideo = document.getElementById('video');
                        mVideo.src = window.URL.createObjectURL(stream);
                        mVideo.play();

                        setTimeout(takeSnapshot, mRefreshTime);


                        qr.callback = function (result, err) {
                            if (err)
                                setTimeout(takeSnapshot, mRefreshTime);
                            callback(err, result);

                        };


                        function takeSnapshot() {
                            var canvas = document.createElement('canvas');
                            var context = canvas.getContext('2d');
                            var w = mVideo.videoWidth;
                            var h = mVideo.videoHeight;
                            canvas.width = w;
                            canvas.height = h;
                            context.drawImage(mVideo, 0, 0, w, h);
                            qr.decode(context.canvas.toDataURL());
                            delete canvas;
                            delete context;
                        }


                    });
                }
            },
            scan_qrcode: function (videoID, refreshTime) {
                mVideo = videoID;
                mRefreshTime = refreshTime || 1000;


                if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {

                    return new Promise(function (resolve) {
                        return navigator.mediaDevices.enumerateDevices().then(function (deviceInfos) {

                                var inputDevices = [];

                                for (var i = 0; i !== deviceInfos.length; ++i) {
                                    var deviceInfo = deviceInfos[i];
                                    if (deviceInfo.kind === 'videoinput') {
                                        inputDevices.push({ID: deviceInfo.deviceId, label: deviceInfo.label});
                                    }
                                }
                                resolve(inputDevices);
                            }
                        )


                    });


                    // .catch(errorCallback);
                }


            }
        }
    };
})(window);
