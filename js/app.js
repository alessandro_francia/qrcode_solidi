$(document).foundation();

var btcUtils = BTCUtils();

//COMMON STUFF
const SPAN_ERROR_SELECTOR = 'span[data-error]';
const SPAN_SUCCESS_SELECTOR = 'span[data-success]';


// SCAN.HTML STUFF
const VIDEO_ID = 'video';
const BUTTON_SCAN_SELECTOR = 'button[data-scan]';
const BUTTON_STOP_SELECTOR = 'button[data-stop]';
const BUTTON_SELECT_SELECTOR = 'button[data-select]';
const MODAL_CAMERA_SELECTOR = '#cameraModal';


$(BUTTON_STOP_SELECTOR).click(stopVideo);


$(BUTTON_SCAN_SELECTOR).click(function () {
    $(BUTTON_STOP_SELECTOR).prop('disabled', false);
    $(BUTTON_SCAN_SELECTOR).prop('disabled', true);

    btcUtils.scan_qrcode(VIDEO_ID, 1000).then(function (inputDevices) {
        $(MODAL_CAMERA_SELECTOR + ' select').empty();
        //check how many camera the device have
        if (inputDevices.length == 1) { //if 1 start the stream
            btcUtils.startStream(inputDevices[0].ID, qrcodeCallback);
        } else {

            // choose the right camera
            inputDevices.forEach(function (deviceInfo) {
                var option = document.createElement('option');
                option.value = deviceInfo.ID;
                option.text = deviceInfo.label || 'camera ' + ($select.length + 1);
                $(MODAL_CAMERA_SELECTOR + ' select').append(option);

                $(MODAL_CAMERA_SELECTOR).foundation('open');

            });
            $(BUTTON_SELECT_SELECTOR).one('click',function () {
                var cameraID = $(MODAL_CAMERA_SELECTOR + ' select').val();
                btcUtils.startStream(cameraID, qrcodeCallback);
                $(MODAL_CAMERA_SELECTOR).foundation('close');
            })

        }
    });


    function qrcodeCallback(err, result) {
        if (err) {
            $(SPAN_ERROR_SELECTOR).text(err).show();
            $(SPAN_SUCCESS_SELECTOR).text('').hide();
        } else {
            $(SPAN_ERROR_SELECTOR).text('').hide();
            result = btcUtils.decode_qrcode_URL(result);
            $(SPAN_SUCCESS_SELECTOR).text(JSON.stringify(result.data)).show();
            stopVideo();
        }
    };
});


function stopVideo() {
    btcUtils.stopVideo();
    $(BUTTON_STOP_SELECTOR).prop('disabled', true);
    $(BUTTON_SCAN_SELECTOR).prop('disabled', false);
}


//GENERATOR.HTML STUFF
const BUTTON_GENERATOR = 'button[data-generator]';

const INPUT_ADDRESS_SELECTOR = 'input[data-name="address"]';
const INPUT_AMOUNT_SELECTOR = 'input[data-name="amount"]';
const INPUT_MESSAGE_SELECTOR = 'input[data-name="message"]';
const INPUT_OTHER_SELECTOR = 'input[data-name="other"]';

const QRCODE_IMAGE_SELECTOR = 'img[data-image]';


$(BUTTON_GENERATOR).click(function () {
    var address = $(INPUT_ADDRESS_SELECTOR).val();
    var amount = $(INPUT_AMOUNT_SELECTOR).val();
    amount = Number(amount);
    var message = $(INPUT_MESSAGE_SELECTOR).val();
    var other = $(INPUT_OTHER_SELECTOR).val();

    var response = btcUtils.encode_qrcode_URL(address, {amount, message, other});
    if (response.errorCode == 0) {
        var data = response.data;
        $(SPAN_ERROR_SELECTOR).text('').hide();
        $(SPAN_SUCCESS_SELECTOR).text(data).show();
        $(QRCODE_IMAGE_SELECTOR).hide();
        $(QRCODE_IMAGE_SELECTOR).attr('src', btcUtils.buildQrcode(data)).show();
    } else {
        $(SPAN_ERROR_SELECTOR).text(response.description).show();
        $(SPAN_SUCCESS_SELECTOR).text('').hide();


    }
    console.log(response);

});

